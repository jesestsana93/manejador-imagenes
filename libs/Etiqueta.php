<?php
/**
 * 
 */
class Etiqueta{
	
	function __construct(){}

	public function altaEtiqueta($id,$marca)
	{
		$db = new MySQL();
		$mensaje = "ok";
		//Verificamos si existe la etiqueta
		$sql = "SELECT * FROM etiquetas ";
		$sql.= "WHERE etiqueta='".$marca."'";
		$r = $db->query($sql);
		if ($r==NULL) {
			//Insertamos la etiqueta
			$sql = "INSERT INTO etiquetas VALUES(0,'".$marca."')";
			if ($db->queryNoSelect($sql)) {
				//Insertar en la tabla pivote
				$idEtiqueta = $db->recuperaId();
				$sql = "INSERT INTO imagenetiqueta VALUES(0,";
				$sql.= $id.",".$idEtiqueta.")";
				if (!$db->queryNoSelect($sql)) {
					$mensaje = "Error al insertar el registro en la tabla pivote";
				}
			} else {
				$mensaje = "Error al insertar el registro en la tabla de etiquetas";
			}
		} else {
			//Ya existe la etiqueta
			$idEtiqueta = $r["id"];
			$sql = "SELECT * FROM imagenetiqueta ";
			$sql.= "WHERE idEtiqueta=".$idEtiqueta." AND ";
			$sql.= "idImagen=".$id;
			$r = $db->query($sql);
			//
			if($r==NULL){
				//Insertar si no existe
				$sql = "INSERT INTO imagenetiqueta VALUES(0,";
				$sql.= $id.",".$idEtiqueta.")";
				if (!$db->queryNoSelect($sql)) {
					$mensaje = "Error al insertar el registro";
				}
			}
		}
		return $mensaje;
	}

	public static function leeEtiquetas()
	{
		$db = new MySQL();
		$sql = "SELECT * FROM etiquetas ";
		$sql.= "ORDER BY etiqueta";
		$data = $db->querySelect($sql);
		$db->close();
		return $data;
	}

	public static function leeEtiquetasImagen($id)
	{
		$db = new MySQL();
		$sql = "SELECT e.etiqueta, e.id ";
		$sql.= "FROM imagenetiqueta as i, etiquetas as e ";
		$sql.= "WHERE i.idImagen=".$id." AND ";
		$sql.= "e.id=i.idEtiqueta";
		$data = $db->querySelect($sql);
		$db->close();
		return $data;
	}

	public static function borraEtiqueta($idImagen, $idEtiqueta)
	{
		$db = new MySQL();
		$sql = "DELETE FROM imagenEtiqueta ";
		$sql.= "WHERE idImagen=".$idImagen." AND ";
		$sql.= "idEtiqueta=".$idEtiqueta;
		$r = $db->queryNoSelect($sql);
		$db->close();
		return $r;
	}

	public static function seleccionarArchivos($marca)
	{
		$db = new MySQL();
		$sql = "SELECT im.id, im.archivo, im.camino, im.size, im.fecha ";
		$sql.= "FROM imagenes as im, imagenEtiqueta as ie, ";
		$sql.= "etiquetas as e ";
		$sql.= "WHERE e.etiqueta='".$marca."' AND ";
		$sql.= "e.id=ie.idEtiqueta AND ";
		$sql.= "ie.idImagen = im.id";

		return $db->querySelect($sql);
	}

	public static function nubeEtiquetas()
	{
		$db = new MySQL();
		$sql = "SELECT e.etiqueta, count(*) as num ";
		$sql.= "FROM imagenEtiqueta as i, ";
		$sql.= "etiquetas as e ";
		$sql.= "WHERE e.id=i.idEtiqueta ";
		$sql.= "GROUP BY e.etiqueta ";
		$sql.= "ORDER BY e.etiqueta";
		$data = $db->querySelect($sql);
		$db->close();
		$etiquetas = [];
		foreach ($data as $valor) {
			$etiquetas[$valor["etiqueta"]] = $valor["num"];
		}
		return $etiquetas;
	}
}