<?php
require "libs/MySQL.php";
require "libs/Etiqueta.php";

$etiquetas_array = Etiqueta::leeEtiquetas();
$nube_array = Etiqueta::nubeEtiquetas();

$etiquetasMenu = true;
$titulo = "Nube etiquetas";
require "php/encabezado.php";
?>
<div class="col-sm-8">
	<h2 class="text-center">Nube etiquetas</h2>
<?php
$max = max($nube_array);
$min = min($nube_array);
$dif = $max - $min;

print "<div class='nube'>";
print "<div class='etiquetas'>";
foreach ($nube_array as $etiqueta => $num) {
	$valorRelativo = round((($num)/$dif)*10);
	//print $valorRelativo.", ".$num.", ".$min.", ".$dif."<br>";
	print "<span class='valor".$valorRelativo."'>";
	print "<a href='seleccionar.php?e=".$etiqueta."'>".$etiqueta."</a>";
	print "</span>";
}
print "</div>";
print "</div>";
print "</div>";
require "php/piepagina.php";
?>
