<?php
/**
 * 
 */
class Imagen{
	
	function __construct(){}

	public static function leeDirectorio()
	{
		$archivo = new RecursiveDirectoryIterator("img");
		$archivo->setFlags(FilesystemIterator::SKIP_DOTS | FilesystemIterator::UNIX_PATHS);
		$archivo = new RecursiveIteratorIterator($archivo, RecursiveIteratorIterator::SELF_FIRST);
		return $archivo;
	}

	public function buscaImagen($data)
	{
		$db = new MySQL();
		//
		$sql = "SELECT id FROM imagenes ";
		$sql.= "WHERE archivo='".$data["archivo"]."' AND ";
		$sql.= "camino='".$data["camino"]."'";
		//print $sql;
		$r = $db->query($sql);
		if ($r==NULL) {
			$sql = "INSERT INTO imagenes VALUES(0,";
			$sql.= "'".$data["archivo"]."', ";
			$sql.= "'".$data["camino"]."', ";
			$sql.= $data["size"].", ";
			$sql.= "'".$data["archivo"]."')";
			if ($db->queryNoSelect($sql)) {
				$id = $db->recuperaId();
			} else {
				$id = NULL;
			}
		} else {
			$id = $r["id"];
		}
		//
		//Crear viñeta / mini
		//
		$mini = quitarExtension($data["archivo"]);
		if(file_exists("mini/".$mini.".jpg")==false){
			Imagen::optimizar($id,$mini,0,"mini",80);
		} 
		$db->close();
		return $id;
	}

	public static function leeImagen($id)
	{
		$db = new MySQL();
		//
		$sql = "SELECT * FROM imagenes ";
		$sql.= "WHERE id=".$id;
		$data = $db->query($sql);
		$db->close();
		return $data;
	}

	public static function borrarImagen($id)
	{
		$data = self::leeImagen($id);
		$db = new MySQL();
		$archivo = $data["camino"]."/".$data["archivo"];
		$mini = "mini/".$data["archivo"];
		if (file_exists($archivo)) {
			unlink($archivo);
		} else {
			print "Error";
			exit();
		}
		if (file_exists($mini)) {
			unlink($mini);
		} else {
			print "Error";
			exit();
		}
		// Borrar de la tabla Imagenes
		$sql = "DELETE FROM imagenes WHERE id=".$id;
		if ($db->queryNoSelect($sql)) {
			$sql = "DELETE FROM imagenEtiqueta ";
			$sql.= "WHERE idImagen=".$id;
			if ($db->queryNoSelect($sql)) {
				return true;
			} 
		}
		return false;
	}

	public static function optimizar($id,$nombre,$op,$carpeta,$nuevoAlto=0)
	{
		$data = self::leeImagen($id);
		//
		$archivo = $data["camino"]."/".$data["archivo"];
		//
		$info = getimagesize($archivo);
		$ancho = $info[0];
		$alto = $info[1];
		$tipo = $info["mime"];
		//
		//Nuevas dimensiones
		//
		if ($op>0) {
			$nuevoAncho = $ancho * $op / 100;
			$nuevoAlto = $alto * $op / 100;
		} else if($nuevoAlto>0){
			$factor = $nuevoAlto / $alto; //80/160=0.5
			$nuevoAncho = $ancho * $factor;
		}
		//
		//Leemos la información del archivo a memoria
		//
		switch ($tipo) {
			case 'image/jpg':
			case 'image/jpeg':
				$imagen = imagecreatefromjpeg($archivo);
				break;

			case 'image/png':
				$imagen = imagecreatefrompng($archivo);
				break;

			case 'image/gif':
				$imagen = imagecreatefromgif($archivo);
				break;
		}

		//Crear el lienzo
		$lienzo = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

		//Preparamos el lienzo para el canal alfa
		if ($tipo=="image/png") {
			imagecolortransparent($lienzo, imagecolorallocatealpha($lienzo, 0, 0, 0, 127));
			imagealphablending($lienzo, false);
			imagesavealpha($lienzo, true);
		}

		//optimizamos
		imagecopyresampled($lienzo, $imagen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
		
		//verificar carpeta
		if (file_exists($carpeta)==false) {
			mkdir($carpeta, 0777, true);
		}

		//Vaciamos al disco
		if($tipo=="image/jpg" || $tipo=="image/jpeg"){
			imagejpeg($lienzo, $carpeta."/".$nombre.".jpg",80);
		} else if($tipo=="image/gif"){
			imagegif($lienzo, $carpeta."/".$nombre.".gif");
		} else if($tipo=="image/png"){
			imagepng($lienzo, $carpeta."/".$nombre.".png");
		} 
		
		//regresamos
		//header("location:index.php");
		return true;
	}

	public function filtrarImagen($id, $filtro, $nombre)
	{
		$data = self::leeImagen($id);
		$archivo = $data["camino"]."/".$data["archivo"];

		$info = getimagesize($archivo);
		$ancho = $info[0];
		$alto = $info[1];
		$tipo = $info["mime"];

		//
		//Leemos la información del archivo a memoria
		//
		switch ($tipo) {
			case 'image/jpg':
			case 'image/jpeg':
				$imagen = imagecreatefromjpeg($archivo);
				break;
			case 'image/png':
				$imagen = imagecreatefrompng($archivo);
				break;
			case 'image/gif':
				$imagen = imagecreatefromgif($archivo);
				break;
		}

		if ($filtro=="negativo") {
			imagefilter($imagen, IMG_FILTER_NEGATE);
		} else if ($filtro=="grises") {
			imagefilter($imagen, IMG_FILTER_GRAYSCALE);
		} else if ($filtro=="rojo") {
			imagefilter($imagen, IMG_FILTER_COLORIZE,100,0,0);
		} else if ($filtro=="verde") {
			imagefilter($imagen, IMG_FILTER_COLORIZE,0,100,0);
		} else if ($filtro=="azul") {
			imagefilter($imagen, IMG_FILTER_COLORIZE,0,0,100);
		} else if ($filtro=="amarillo") {
			imagefilter($imagen, IMG_FILTER_COLORIZE,100,100,-100);
		} else if ($filtro=="brillo") {
			imagefilter($imagen, IMG_FILTER_BRIGHTNESS,50);
		} else if ($filtro=="contraste") {
			imagefilter($imagen, IMG_FILTER_CONTRAST,20);
		} else if ($filtro=="sepia") {
			imagefilter($imagen, IMG_FILTER_GRAYSCALE);
			imagefilter($imagen, IMG_FILTER_COLORIZE,100,70,50);
		} else if ($filtro=="contornos") {
			imagefilter($imagen, IMG_FILTER_EDGEDETECT);
		} else if ($filtro=="emboss") {
			imagefilter($imagen, IMG_FILTER_EMBOSS);
		} else if ($filtro=="gauss") {
			for ($i=0; $i < 40 ; $i++) { 
				imagefilter($imagen, IMG_FILTER_GAUSSIAN_BLUR);
			}
			imagefilter($imagen, IMG_FILTER_SMOOTH,-7);
		} else if ($filtro=="selectivo") {
			imagefilter($imagen, IMG_FILTER_SELECTIVE_BLUR);
		} else if ($filtro=="removal") {
			imagefilter($imagen, IMG_FILTER_MEAN_REMOVAL);
		} else if ($filtro=="suavizado") {
			imagefilter($imagen, IMG_FILTER_SMOOTH,-7);
		} else if ($filtro=="pixelado") {
			imagefilter($imagen, IMG_FILTER_PIXELATE, 10, true);
		} 

		$lienzo = imagecreatetruecolor($ancho, $alto);

		//Preparamos el lienzo para el canal alfa
		if ($tipo=="image/png") {
			imagecolortransparent($lienzo, imagecolorallocatealpha($lienzo, 0, 0, 0, 127));
			imagealphablending($lienzo, false);
			imagesavealpha($lienzo, true);
		}

		//Copiamos la nueva imagen al lienzo
		imagecopy($lienzo, $imagen, 0, 0, 0, 0, $ancho, $alto);

		//verificar carpeta
		$carpeta = "img/nuevas/";
		if (file_exists($carpeta)==false) {
			mkdir($carpeta, 0777, true);
		}

		//Vaciamos al disco
		if($tipo=="image/jpg" || $tipo=="image/jpeg"){
			imagejpeg($lienzo, $carpeta."/".$nombre.".jpg",80);
		} else if($tipo=="image/gif"){
			imagegif($lienzo, $carpeta."/".$nombre.".gif");
		} else if($tipo=="image/png"){
			imagepng($lienzo, $carpeta."/".$nombre.".png");
		} 

		imagedestroy($imagen);
		//regresamos
		return true;	
	}

	public function rotarImagen($id, $grados, $nombre)
	{
		$data = self::leeImagen($id);
		$archivo = $data["camino"]."/".$data["archivo"];

		$info = getimagesize($archivo);
		$ancho = $info[0];
		$alto = $info[1];
		$tipo = $info["mime"];

		//
		//Leemos la información del archivo a memoria
		//
		switch ($tipo) {
			case 'image/jpg':
			case 'image/jpeg':
				$imagen = imagecreatefromjpeg($archivo);
				break;
			case 'image/png':
				$imagen = imagecreatefrompng($archivo);
				break;
			case 'image/gif':
				$imagen = imagecreatefromgif($archivo);
				break;
		}

		//Rotar imagen
		$lienzo = imagerotate($imagen, $grados,0);

		//verificar carpeta
		$carpeta = "img/nuevas/";
		if (file_exists($carpeta)==false) {
			mkdir($carpeta, 0777, true);
		}

		//Vaciamos al disco
		if($tipo=="image/jpg" || $tipo=="image/jpeg"){
			imagejpeg($lienzo, $carpeta."/".$nombre.".jpg",80);
		} else if($tipo=="image/gif"){
			imagegif($lienzo, $carpeta."/".$nombre.".gif");
		} else if($tipo=="image/png"){
			imagepng($lienzo, $carpeta."/".$nombre.".png");
		} 

		//Borramos de memoria el recurso
		imagedestroy($imagen);

		//regresamos
		return true;	
	}

	public function voltearImagen($id, $modo, $nombre)
	{
		$data = self::leeImagen($id);
		$archivo = $data["camino"]."/".$data["archivo"];

		$info = getimagesize($archivo);
		$ancho = $info[0];
		$alto = $info[1];
		$tipo = $info["mime"];

		//
		//Leemos la información del archivo a memoria
		//
		switch ($tipo) {
			case 'image/jpg':
			case 'image/jpeg':
				$imagen = imagecreatefromjpeg($archivo);
				break;
			case 'image/png':
				$imagen = imagecreatefrompng($archivo);
				break;
			case 'image/gif':
				$imagen = imagecreatefromgif($archivo);
				break;
		}

		//Rotar imagen
		if ($modo=="vertical") {
			imageflip($imagen, IMG_FLIP_VERTICAL);
		} else if ($modo=="horizontal") {
			imageflip($imagen, IMG_FLIP_HORIZONTAL);
		} else {
			imageflip($imagen, IMG_FLIP_BOTH);
		}

		//verificar carpeta
		$carpeta = "img/nuevas/";
		if (file_exists($carpeta)==false) {
			mkdir($carpeta, 0777, true);
		}

		//Vaciamos al disco
		if($tipo=="image/jpg" || $tipo=="image/jpeg"){
			imagejpeg($imagen, $carpeta."/".$nombre.".jpg",80);
		} else if($tipo=="image/gif"){
			imagegif($imagen, $carpeta."/".$nombre.".gif");
		} else if($tipo=="image/png"){
			imagepng($imagen, $carpeta."/".$nombre.".png");
		} 

		//Borramos de memoria el recurso
		imagedestroy($imagen);

		//regresamos
		return true;	
	}
}




