<?php
require "libs/MySQL.php";
require "libs/Imagen.php";

if (isset($_GET["id"])) {
	$id = $_GET["id"];
} else if(isset($_POST["id"])){
	$id = $_POST["id"];
	$grados = $_POST["grados"];
	$nombre = $_POST["nombre"];
	//proceso de borrado
	if ($grados!="" && $nombre!="") {
		if (Imagen::rotarImagen($id,$grados,$nombre)) {
			header("location:index.php");
		} else {
			print "Existió un problema al aplicar un filtro la imagen<br>";
		}
	}
} else {
	header("location:index.php");
}

$data = Imagen::leeImagen($id);
$etiquetasMenu = false;
$titulo = "Rotar una Imagen";
require "php/encabezado.php";
?>
<div class="col-sm-5">
	<?php
		$archivo = $data["camino"]."/".$data["archivo"];
		print "<img src='".$archivo."' width='100%'/>";
	?>
</div>
<div class="col-sm-3">
	<h3 class="text-center">Rotar una imagen</h3>
	<?php
	$img = $data["camino"]."/".$data["archivo"];
	$imagen = getimagesize($img);
	$ancho = $imagen[0];
	$alto = $imagen[1];

	print "<table>";
	print "<tr><td>id: </td><td>".$id."</td></tr>";
	print "<tr><td>Archivo: </td><td>".$data["archivo"]."</td></tr>";
	print "<tr><td>Camino: </td><td>".$data["camino"]."</td></tr>";
	print "<tr><td>Tamaño: </td><td>".$data["size"]." kb</td></tr>";
	print "<tr><td>Ancho: </td><td>".number_format($ancho,0)."px </td></tr>";
	print "<tr><td>Alto: </td><td>".number_format($alto,0)."px </td></tr>";
	print "<tr><td>Fecha: </td><td>".date("Y/m/d",$data["fecha"])."</td></tr>";
	print "</table>";
	print "<br>";
	?>
	<form action="rotar.php" method="post">
		<select id="grados" name="grados">
			<option value="">Seleccione los grados a rotar</option>
			<option value="90">90%</option>
			<option value="180">180%</option>
			<option value="270">270%</option>
		</select>
		<br><br>
		<label for="nombre">Nombre de la nueva imagen:</label>
		<br><br>
		<input type="text" name="nombre" id="nombre"/>
		<input type="hidden" name="id" id="id" value="<?php print $id; ?>">
		<br><br>
		<input type="submit" value="Rotar" class="btn btn-info btn-block">
	</form>
	<br>
	<a class='btn btn-success btn-block' href='caratula.php?id=".$id."'>Regresar</a>
</div>
<?php
require "php/piepagina.php";
?>