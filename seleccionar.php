<?php
require "libs/MySQL.php";
require "libs/Etiqueta.php";
if (isset($_POST["marca"]) || isset($_GET["e"])) {
	if (isset($_POST["marca"])) {
		$marca = $_POST["marca"] ;
	} else {
		$marca = $_GET["e"] ;
	}

	if ($marca=="") {
		header("location:index.php");
	} else {
		$archivos_array = Etiqueta::seleccionarArchivos($marca);
	}
} else {
	header("location:index.php");
}

$etiquetas_array = Etiqueta::leeEtiquetas();
$etiquetasMenu = true;
$titulo = "Filtrar imágenes";
require "php/encabezado.php";
?>
<div class="col-sm-8">
	<h2 class="text-center">Imágenes con <?php print $marca."(".count($archivos_array).")"; ?></h2>
<?php
$fotos_array = [];
foreach ($archivos_array as $archivo) {
	$img = $archivo["camino"]."/".$archivo["archivo"];
	$mini = "mini/".$archivo["archivo"];
	$foto = [
		"archivo"=>$archivo["archivo"],
		"camino"=>$archivo["camino"],
		"size"=>$archivo["size"],
		"fecha"=>$archivo["fecha"]
	];
	$foto["id"] = $archivo["id"];
	$fotos_array[] = $foto;
	if (file_exists($mini)) {
		print "<img id='' src='".$mini."' onClick=selecciona(".$foto["id"].") />";
	} else {
		print "<img id='' src='".$img."' height='80' onClick=selecciona(".$foto["id"].") />";
	}
}
print "</div>";
require "php/piepagina.php";
?>
